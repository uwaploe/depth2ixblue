module bitbucket.org/uwaploe/depth2ixblue

go 1.15

require (
	bitbucket.org/uwaploe/go-focus v0.4.1
	bitbucket.org/uwaploe/ixblue v0.2.2
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/nats-io/nats-server/v2 v2.2.0 // indirect
	github.com/nats-io/nats-streaming-server v0.21.1 // indirect
	github.com/nats-io/stan.go v0.8.3
)
