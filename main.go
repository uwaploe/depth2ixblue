// Depth2ixblue sends depth data to an iXBlue Rovins INS
package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	focus "bitbucket.org/uwaploe/go-focus"
	"bitbucket.org/uwaploe/ixblue"
	stan "github.com/nats-io/stan.go"
)

const Usage = `Usage: depth2ixblue [options] host:port

Subscribe to the depth data provided by the FOCUS vehicle and generate ixBlue
external sensor messages to send to the Rovins INS at HOST:PORT.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	debugMode = flag.Bool("debug", false,
		"Log debugging output")
	natsURL      string = "nats://localhost:4222"
	clusterID    string = "must-cluster"
	focusSubject string = "nmea.focus"
	protocol     string = "tcp"
	stdDev       float64
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&focusSubject, "sub", lookupEnvOrString("FOCUS_SUBJECT", focusSubject),
		"Subject name for INS data")
	flag.StringVar(&protocol, "proto", protocol, "Network protocol for Rovins input")
	flag.Float64Var(&stdDev, "std", stdDev, "standard deviation for depth value")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	rovinsAddr := args[0]

	var status int
	defer func() { os.Exit(status) }()

	sc, err := stan.Connect(clusterID, "rovins-depth", stan.NatsURL(natsURL))
	if err != nil {
		log.Printf("Cannot connect: %v", err)
		status = 1
		return
	}
	defer sc.Close()

	conn, err := net.Dial(protocol, rovinsAddr)
	if err != nil {
		log.Print("Cannot access Rovins")
		status = 1
		return
	}

	focusCb := func(m *stan.Msg) {
		var sentence string
		// Accept tagged or untagged NMEA messages produced by nmeapub.
		bs := bytes.SplitN(m.Data, []byte(":"), 2)
		if len(bs) == 2 {
			sentence = string(bs[1])
		} else {
			sentence = string(bs[0])
		}
		rec, err := focus.ParseRecord(sentence)
		if err != nil {
			log.Printf("FOCUS record parse error: %v", err)
		}
		enc := ixblue.NewEncoder(conn)
		block := ixblue.DepthData{
			Tvalid: ixblue.Ticks(time.Now().UTC()),
			Depth:  rec["depth"],
			Stddev: float32(stdDev),
		}
		if err := enc.Encode(&block); err != nil {
			log.Printf("Error sending depth data: %v", err)
		}
	}

	sub, err := sc.Subscribe(focusSubject, focusCb)
	if err != nil {
		log.Fatalf("Cannot subscribe to FOCUS data: %v", err)
	}
	defer sub.Unsubscribe()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	log.Printf("Rovins depth service starting %s", Version)

	// Exit on a signal
	<-sigs
}
